package com.rkritchat.hibernate.user.service.impl;

import org.springframework.stereotype.Component;

import com.rkritchat.hibernate.configuration.DependencyInjection;
import com.rkritchat.hibernate.user.bean.UserInfoBean;
import com.rkritchat.hibernate.user.dao.UserDao;
import com.rkritchat.hibernate.user.service.UserService;

/**
 * @author rkritchat
 * @date 13-03-2561
 */
@Component
public class UserServiceImpl implements UserService {
	private UserDao userDao;
	
	public String createUser(UserInfoBean bean) {
		try {
			userDao = DependencyInjection.getContext().getBean(UserDao.class);
			return userDao.createUser(bean);
		} catch (Exception exception) {
			System.out.println(exception);
			return "Faile while create user";
		}
	}
	
	public String getUser(String id) {
		try {
			userDao = DependencyInjection.getContext().getBean(UserDao.class);
			return userDao.getUserDetail(id);
		} catch (Exception exception) {
			System.out.println(exception);
			return "Faile while get user detail";
		}
	}

	public String deleteUser(UserInfoBean bean) {
		try {
			userDao = DependencyInjection.getContext().getBean(UserDao.class);
			return userDao.deleteUser(bean);
		}catch(Exception exception) {
			System.out.println(exception);
			return "File while delete user";
		}
	}

	public String updateUserDetail(UserInfoBean bean){
		try {
			userDao = DependencyInjection.getContext().getBean(UserDao.class);
			return userDao.updateUserDetail(bean);
		}catch(Exception exception) {
			System.out.println(exception);
			return "File while delete user";
		}
	}

}
