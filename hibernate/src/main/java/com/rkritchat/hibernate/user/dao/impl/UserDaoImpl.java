package com.rkritchat.hibernate.user.dao.impl;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

import com.rkritchat.hibernate.connection.ConnectionDB;
import com.rkritchat.hibernate.user.bean.UserInfoBean;
import com.rkritchat.hibernate.user.dao.UserDao;

/**
 * @author rkritchat
 * @date 13-03-2561
 */
@Component
public class UserDaoImpl implements UserDao{
	
	public String createUser(UserInfoBean bean) throws Exception{
		ConnectionDB.getConfiguration().addAnnotatedClass(UserInfoBean.class);
		SessionFactory factory = ConnectionDB.getSessionFactory();
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			session.save(bean);
			session.getTransaction().commit();
		}finally {
			factory.close();
		}
		
		return "Created user successfully.";
	}
	
	public String getUserDetail(String id) throws Exception {
		ConnectionDB.getConfiguration().addAnnotatedClass(UserInfoBean.class);
		SessionFactory factory = ConnectionDB.getSessionFactory();
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			return session.get(UserInfoBean.class, id).toString();
		}finally {
			factory.close();
		}
	}

	public String deleteUser(UserInfoBean bean) throws Exception {
		ConnectionDB.getConfiguration().addAnnotatedClass(UserInfoBean.class);
		SessionFactory factory = ConnectionDB.getSessionFactory();
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			session.delete(bean);
			session.getTransaction().commit();
		}finally {
			factory.close();
		}
		return "Delete user Successfully";
	}

	public String updateUserDetail(UserInfoBean bean) throws Exception {
		
		ConnectionDB.getConfiguration().addAnnotatedClass(UserInfoBean.class);
		SessionFactory factory = ConnectionDB.getSessionFactory();
		try {
			Session session = factory.getCurrentSession();
			session.beginTransaction();
			Query query = session.createQuery("UPDATE UserInfoBean SET firstName = :name WHERE id = :id ");
			query.setParameter("name", bean.getFirstName());
			query.setParameter("id", bean.getId());
			query.executeUpdate();			
			session.getTransaction().commit();
		}finally {
			factory.close();
		}
		
		return "Update user detail Successfully";
	}

}
