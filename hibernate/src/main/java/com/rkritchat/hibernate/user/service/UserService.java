package com.rkritchat.hibernate.user.service;

import com.rkritchat.hibernate.user.bean.UserInfoBean;

public interface UserService {
	
	/**
	 * This service for create User
	 * @param bean
	 * @return
	 */
	public String createUser(UserInfoBean bean);
	
	/**
	 * This service for get User detail
	 * @param id
	 * @return
	 */
	public String getUser(String id);
	
	/**
	 * This service for delete user
	 * @param bean
	 * @return
	 */
	public String deleteUser(UserInfoBean bean);

	/**
	 * This serivce for update user detial
	 * @return
	 * @throws Exception
	 */
	public String updateUserDetail(UserInfoBean bean);

}
