package com.rkritchat.hibernate.user.dao;

import com.rkritchat.hibernate.user.bean.UserInfoBean;

public interface UserDao {
	/**
	 * This method for create user
	 * @param bean
	 * @return String
	 * @throws Exception
	 */
	public String createUser(UserInfoBean bean) throws Exception;
	
	/**
	 * This method for get user detail
	 * @param id
	 * @return String
	 * @throws Exception
	 */
	public String getUserDetail(String id) throws Exception;
	
	/**
	 * This method for delete user
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	public String deleteUser(UserInfoBean bean) throws Exception;
	
	/**
	 * This method for update user's detail
	 * @return
	 * @throws Exception
	 */
	public String updateUserDetail(UserInfoBean bean) throws Exception;
}
