package com.rkritchat.hibernate.connection;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author rkritchat
 * @date 13-03-2561
 * This Class initial Hibernate Configuration
 */
public class ConnectionDB {
	  public static Configuration configuration = new Configuration().configure();
	  
	  public static SessionFactory getSessionFactory() {
		  return configuration.buildSessionFactory();
	  }
	  
	  public static Configuration getConfiguration() {
		  return configuration;
	  }
}
