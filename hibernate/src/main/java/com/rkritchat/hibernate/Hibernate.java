package com.rkritchat.hibernate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.rkritchat.hibernate.configuration.ApplicationConfiguration;
import com.rkritchat.hibernate.user.bean.UserInfoBean;
import com.rkritchat.hibernate.user.service.UserService;
import com.rkritchat.hibernate.user.service.impl.UserServiceImpl;

/**
 * @author rkritchat
 * @date 13-03-2561
 * This main show how to use Hibernate and Spring Context (base on Java Annotation)
 */
public class Hibernate 
{	
    public static void main( String[] args )
    {
    		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
    		UserService userService = context.getBean(UserServiceImpl.class);
    		UserInfoBean userBean = new UserInfoBean("rkritchat","Kritchat2","Rojanaphruk","rkritchat@gmail.com");
    		updateUserDetil(userService, userBean);
	
    }
    
    public static void creatUser(UserService userService, UserInfoBean userBean) {
		userService.createUser(userBean);
    	}
    
    public static void getUserDetail(UserService userService) {
    		String userDetail = userService.getUser("rkritchat");
		System.out.println(userDetail);
    }
    
    public static void deleteUser(UserService userService, UserInfoBean userBean) {
    		System.out.println(userService.deleteUser(userBean));
    }
    
    public static void updateUserDetil(UserService userService, UserInfoBean userBean) {
    		System.out.println(userService.updateUserDetail(userBean));
    }
}
