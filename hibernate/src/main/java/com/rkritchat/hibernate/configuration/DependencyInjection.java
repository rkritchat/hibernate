package com.rkritchat.hibernate.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * @author rkritchat
 * @date 13-03-2561
 * This Class for make the dependency injection
 */
public class DependencyInjection {
	public static ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
	public static ApplicationContext getContext() {
		return context;
	}
}
