package com.rkritchat.hibernate.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author rkritchat
 * @date 13-03-2561
 * This configuration method apply ComponentScan
 */
@Configuration
@ComponentScan("com.rkritchat")
public class ApplicationConfiguration {

}
